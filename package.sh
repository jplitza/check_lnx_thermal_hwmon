#!/bin/bash

AUTHOR="thierry.tra@hostmail.com"
TITLE="Linux Thermals using /sys/class/hwmon"
PKG_NAME="lnx_thermals_hwmon"
DESCRIPTION="This is a custom check plugin to support monitoring linux thermals from /sys/class/hwmon/hwmon* instead of the default by checkmk. It sends data compatible with the default checkmk check lnx_thermals. **NOTE** that this check may not be compatible with systems that already send data the checkmk-way."
PLUGIN_VERSION="1.1"
CHECKMK_MIN_VERSION="2.0.0"
CHECKMK_PKG_VERSION=$CHECKMK_MIN_VERSION
DOWNLOAD_URL="https://bitbucket.org/Nerothank/check_lnx_thermal_hwmon"

SOURCE_PATH=$(pwd)
OUTPUT_PATH=$(pwd)

# remove any python compiled binaries
find . -name "*.pyc" -delete

# STOP EDITING HERE
current_dir=$(pwd)
cd "$SOURCE_PATH"

declare -A files

# collect all files in all directories and tar them separately
while read dir; do
    cd "$dir"

    ## find relevant files
    file_list="$(find -L -type f)"

    ## skip folders without files
    if [ ! -z "$file_list" ]; then
        ## update file dictionary
        files[$dir]="$file_list"

        ## tar files
        tar --dereference -cf "$OUTPUT_PATH/$dir.tar" *
    fi

    cd - &>/dev/null
done < <(find . -maxdepth 1 -type d -name "[!.]*")

# construct the info dictionary/JSON
filedict=""
numfiles=0
## output file list for every dir
for key in "${!files[@]}"; do
    pretty_key=$(echo $key | sed -E 's/\.\///g')

    ## start list -> 'key: [
    filedict="$filedict'$pretty_key': ["

    ## add entries -> 'entry',
    for file in ${files[$key]}; do
        pretty_file=$(echo $file | sed -E 's/^\.\///g')
        filedict="$filedict'$pretty_file',"
        numfiles=$(( $numfiles + 1 ))
    done

    ## remove trailing comma
    filedict="${filedict::-1}"

    ## close list -> ],
    filedict="$filedict],"
done

## remove trailing comma
filedict="${filedict::-1}"

# finalize info dict/JSON
info="{'author': '$AUTHOR',
 'description': '${DESCRIPTION/\'/\\\'}',
 'download_url': '$DOWNLOAD_URL',
 'files': {$filedict},
 'name': '$PKG_NAME',
 'num_files': $numfiles,
 'title': '$TITLE',
 'version': '$PLUGIN_VERSION',
 'version.min_required': '$CHECKMK_MIN_VERSION',
 'version.packaged': '$CHECKMK_PKG_VERSION'}"

cd "$OUTPUT_PATH"

echo $info > info
echo $info | sed -E "s/'/"'"'"/g" > info.json

tar -c *.tar info info.json | gzip --best - > "$OUTPUT_PATH/$PKG_NAME.mkp"

# cleanup temp files
rm -f *.tar info info.json

cd $current_dir
