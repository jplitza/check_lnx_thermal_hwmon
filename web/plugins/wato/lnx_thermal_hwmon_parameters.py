#!/usr/bin/env python3

from cmk.gui.valuespec import (
    DropdownChoice,
)
from cmk.gui.plugins.wato import (
    HostRulespec,
    rulespec_registry,
)

from cmk.utils.version import is_raw_edition
try:
    from cmk.gui.cee.plugins.wato.agent_bakery.rulespecs.utils import RulespecGroupMonitoringAgentsAgentPlugins
except ImportError:
    # we are running raw edition, there is no bakery
    pass

###### BAKERY RULE
if not is_raw_edition():
    def _bakery_valuespec_lnx_thermal_hwmon():
        return DropdownChoice(
            title=_("Monitor thermals using /sys/class/hwmon (Linux)"),
            help=_("This will deploy the agent plugin <tt>lnx_thermal_hwmon.sh</tt>."),
            choices=[
                (True, _("Deploy plugin lnx_thermal_hwmon")),
                (None, _("Do not deploy plugin lnx_thermal_hwmon")),
            ],
            default_value=None
        )

    rulespec_registry.register(
        HostRulespec(
            group=RulespecGroupMonitoringAgentsAgentPlugins,
            name="agent_config:lnx_thermal_hwmon",
            valuespec=_bakery_valuespec_lnx_thermal_hwmon,
            match_type="first",# first => only first rule matches, all => all rules match and define parameters, dict => all rules match and override each other
        ))
