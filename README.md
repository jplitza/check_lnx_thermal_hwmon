# What is this? #

This is a custom check plugin to support monitoring linux thermals from `/sys/class/hwmon/hwmon*` instead of the default by checkmk.

# Installation / Usage #

Use the packaging script, install on the checkmk site and then deploy the plugin `lnx_thermal_hwmon.sh` to your linux server(s).